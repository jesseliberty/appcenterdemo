﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace AppCenterDemoUITests
{
    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class Tests
    {
        IApp app;
        Platform platform;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        [Test]
        public void AppLaunches()
        {
            app.Screenshot("First screen.");
        }

        [Test]
        public void AboutPage()
        {
            // app.Repl();
            app.Tap(x => x.Text("About"));
            app.Tap(x => x.Text("Browse"));
            app.WaitForElement(x => x.Text("First item"));
            var AppExists = app.Query("First item").Any();
            Assert.IsTrue(AppExists);
        }
    }
}

