﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppCenterDemo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AboutPage : ContentPage
	{
		public AboutPage ()
		{
			InitializeComponent ();
		}
	}
}